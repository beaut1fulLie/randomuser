package ru.mrgeng.randomuser.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import ru.mrgeng.randomuser.models.QueryData;
import ru.mrgeng.randomuser.models.User;
import ru.mrgeng.randomuser.repository.*;

import java.io.*;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

/**
 * Сервис для сохранения и вывода данных о пользователе
 */
@Service
public class UserService {

    @Autowired
    private InfoRepository infoRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private IdentificatorRepository identificatorRepository;
    @Autowired
    private CoordinatesRepository coordinatesRepository;
    @Autowired
    private DobRepository dobRepository;
    @Autowired
    private LocationRepository locationRepository;
    @Autowired
    private LoginRepository loginRepository;
    @Autowired
    private NameRepository nameRepository;
    @Autowired
    private PictureRepository pictureRepository;
    @Autowired
    private RegisterInfoRepository registerInfoRepository;
    @Autowired
    private StreetRepository streetRepository;
    @Autowired
    private TimeZoneRepository timeZoneRepository;

    private static final String serviceUrl = "https://api.randomuser.me/?results=";
    /**
     * Список заголовков для конвертации данных в файл
     */
     private static final String[] headersCsv = {"gender","title", "first name", "last name","street number","street name",
             "city","state", "country", "postcode","latitude","longitude",
             "timezone offset","timezone description", "email", "uuid","username","password", "salt","md5", "sha1",
             "sha256","date of birth","current age", "register date", "register age","phone","cell","name id",
             "value id","large picture url","medium picture url", "thumbnail url","nat"};

    /**
     * Метод для получения и сохранения в базу данных информации о пользователях из сервиса
     * @param entityNumber количество записей, которые необходимо получить с сервиса
     * @throws InterruptedException
     */
    public void getUsers(int entityNumber) throws InterruptedException {
        Scanner sc = new Scanner(System.in);
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<String>(headers);
        try {
            ResponseEntity<QueryData> response = restTemplate.exchange(
                    serviceUrl + entityNumber, HttpMethod.GET, entity,
                    new ParameterizedTypeReference<QueryData>() {
                    });
            List<User> results = Objects.requireNonNull(response.getBody()).getResults();
            saveUserData(results);
            System.out.println("Данные успешно получены. Нажмите Enter, чтобы вернутся в меню");

        } catch (RestClientException exception) {
            System.out.println("Ошибка обработки запроса! Попробуйте повторить операцию позже");
            return;
        }


    }

    /**
     * Метод, сохраняющий данные о пользователе в соответствующие сущности
     * TODO При появлении в будущем действий со связанными сущностями реализовать для них отдельный сервис и сохранять через него
     * @param data Список пользователей, передаваемый для сохранения в базу данных
     */
    private void saveUserData(List<User> data) {
        for (int i = 0; i < data.size();i++) {
            User user = data.get(i);
            streetRepository.save(user.getLocation().getStreet());
            coordinatesRepository.save(user.getLocation().getCoordinates());
            dobRepository.save(user.getDob());
            timeZoneRepository.save(user.getLocation().getTimezone());
            pictureRepository.save(user.getPicture());
            nameRepository.save(user.getName());
            locationRepository.save(user.getLocation());
            identificatorRepository.save(user.getId());
            loginRepository.save(user.getLogin());
            registerInfoRepository.save(user.getRegistered());
            userRepository.save(user);
        }
    }

    /**
     * Метод для экспорта передаваемого количества записей о пользователе в формат csv
     * @param count Количество записей для экспорт в csv
     * @throws IOException
     * @throws InterruptedException
     */
    public void writeUsersToCsv(int count) throws IOException, InterruptedException {
        try {
            FileWriter out = new FileWriter("users.csv");
            List<User> users = userRepository.findAll(PageRequest.of(0, count)).toList();
            if (userRepository.count() < count) {
                System.out.println("Предупреждение: количество записей в базе меньше введенного параметра");
            }
            PrintWriter printWriter = new PrintWriter(out);
            String headerTest = String.join(",", headersCsv);
            printWriter.println(headerTest);
            users.forEach((x) -> printWriter.println(String.join(",", x.toList())));
            System.out.println("Данные сохранены в файл user.csv! Нажмите Enter, чтобы вернутся в меню");
            out.close();
        } catch (IOException e) {
            System.out.println("Не удалось открыть файл для записи! Возможно он открыт в стороннем приложении." +
                    " Нажмите Enter для продолжения");
        }
    }
}
