package ru.mrgeng.randomuser.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.mrgeng.randomuser.models.Dob;

@Repository
public interface DobRepository extends JpaRepository<Dob, Long> {
}