package ru.mrgeng.randomuser.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.mrgeng.randomuser.models.Name;

@Repository
public interface NameRepository extends JpaRepository<Name, Long> {
}