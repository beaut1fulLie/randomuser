package ru.mrgeng.randomuser.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.mrgeng.randomuser.models.Info;

@Repository
public interface InfoRepository extends JpaRepository<Info, Long> {
}
