package ru.mrgeng.randomuser.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.mrgeng.randomuser.models.Street;

@Repository
public interface StreetRepository extends JpaRepository<Street, Long> {
}