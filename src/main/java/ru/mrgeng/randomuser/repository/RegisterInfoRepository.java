package ru.mrgeng.randomuser.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.mrgeng.randomuser.models.RegisterInfo;

@Repository
public interface RegisterInfoRepository extends JpaRepository<RegisterInfo, Long> {
}