package ru.mrgeng.randomuser.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.mrgeng.randomuser.models.Location;

@Repository
public interface LocationRepository extends JpaRepository<Location, Long> {
}