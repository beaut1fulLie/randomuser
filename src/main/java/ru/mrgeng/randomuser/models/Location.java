package ru.mrgeng.randomuser.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;


@Data
@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class Location {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long locationId;

    private String city;
    private String state;
    private String country;
    private String postcode;
    @OneToOne
    @JoinColumn(name = "street_id")
    private Street street;
    @OneToOne
    @JoinColumn(name = "coordinates_id")
    private Coordinates coordinates;
    @OneToOne
    @JoinColumn(name = "timezone_id")
    private TimeZone timezone;

}
