package ru.mrgeng.randomuser.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "user_id", nullable = false)
    private Long userId;

    private String gender;
    @OneToOne
    @JoinColumn(name = "name_id")
    private Name name;
    @OneToOne
    @JoinColumn(name = "location_id")
    private Location location;
    private String email;
    @OneToOne
    @JoinColumn(name = "login_id")
    private Login login;
    @OneToOne
    @JoinColumn(name = "dob_id")
    private Dob dob;
    @OneToOne
    @JoinColumn(name = "registered_id")
    private RegisterInfo registered;
    private String phone;
    private String cell;
    @OneToOne
    @JoinColumn(name = "id_id")
    private Identificator id;
    @OneToOne
    @JoinColumn(name = "picture_id")
    private Picture picture;
    private String nat;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public List<String> toList () {
        List<String> result = new ArrayList<>();
        result.add(gender);
        result.add(name.getTitle());
        result.add(name.getFirst());
        result.add(name.getLast());
        result.add(location.getStreet().getNumber().toString());
        result.add(location.getStreet().getName());
        result.add(location.getCity());
        result.add(location.getState());
        result.add(location.getCountry());
        result.add(location.getPostcode());
        result.add(location.getCoordinates().getLatitude());
        result.add(location.getCoordinates().getLongitude());
        result.add(location.getTimezone().getOffset());
        result.add(location.getTimezone().getDescription());
        result.add(email);
        result.add(login.getUuid());
        result.add(login.getUsername());
        result.add(login.getPassword());
        result.add(login.getSalt());
        result.add(login.getMd5());
        result.add(login.getSha1());
        result.add(login.getSha256());
        result.add(dob.getDate().toString());
        result.add(dob.getAge().toString());
        result.add(registered.getDate().toString());
        result.add(registered.getAge().toString());
        result.add(phone);
        result.add(cell);
        result.add(id.getName());
        result.add(id.getValue());
        result.add(picture.getLarge());
        result.add(picture.getMedium());
        result.add(picture.getThumbnail());
        result.add(nat);
        return result;
    }
    public String toStringWithDelimiter(Character delimiter) {
        StringBuilder stringBuffer = new StringBuilder();
        stringBuffer.append(gender);
        stringBuffer.append(delimiter);
        stringBuffer.append(name.getTitle());
        stringBuffer.append(delimiter);
        stringBuffer.append(name.getFirst());
        stringBuffer.append(delimiter);
        stringBuffer.append(name.getLast());
        stringBuffer.append(delimiter);
        stringBuffer.append(location.getStreet().getNumber());
        stringBuffer.append(delimiter);
        stringBuffer.append(location.getStreet().getName());
        stringBuffer.append(delimiter);
        stringBuffer.append(location.getCity());
        stringBuffer.append(delimiter);
        stringBuffer.append(location.getState());
        stringBuffer.append(delimiter);
        stringBuffer.append(location.getCountry());
        stringBuffer.append(delimiter);
        stringBuffer.append(location.getPostcode());
        stringBuffer.append(delimiter);
        stringBuffer.append(location.getCoordinates().getLatitude());
        stringBuffer.append(delimiter);
        stringBuffer.append(location.getCoordinates().getLongitude());
        stringBuffer.append(delimiter);
        stringBuffer.append(location.getTimezone().getOffset());
        stringBuffer.append(delimiter);
        stringBuffer.append(location.getTimezone().getDescription());
        stringBuffer.append(delimiter);
        stringBuffer.append(email);
        stringBuffer.append(delimiter);
        stringBuffer.append(login.getUuid());
        stringBuffer.append(delimiter);
        stringBuffer.append(login.getUsername());
        stringBuffer.append(delimiter);
        stringBuffer.append(login.getPassword());
        stringBuffer.append(delimiter);
        stringBuffer.append(login.getSalt());
        stringBuffer.append(delimiter);
        stringBuffer.append(login.getMd5());
        stringBuffer.append(delimiter);
        stringBuffer.append(login.getSha1());
        stringBuffer.append(delimiter);
        stringBuffer.append(login.getSha256());
        stringBuffer.append(delimiter);
        stringBuffer.append(dob.getDate());
        stringBuffer.append(delimiter);
        stringBuffer.append(dob.getAge());
        stringBuffer.append(delimiter);
        stringBuffer.append(registered.getDate());
        stringBuffer.append(delimiter);
        stringBuffer.append(registered.getAge());
        stringBuffer.append(delimiter);
        stringBuffer.append(phone);
        stringBuffer.append(delimiter);
        stringBuffer.append(cell);
        stringBuffer.append(delimiter);
        stringBuffer.append(id.getName());
        stringBuffer.append(delimiter);
        stringBuffer.append(id.getValue());
        stringBuffer.append(delimiter);
        stringBuffer.append(picture.getLarge());
        stringBuffer.append(delimiter);
        stringBuffer.append(picture.getMedium());
        stringBuffer.append(delimiter);
        stringBuffer.append(picture.getThumbnail());
        stringBuffer.append(delimiter);
        stringBuffer.append(nat);
        return stringBuffer.toString();
    }

}
