package ru.mrgeng.randomuser.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Name {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long nameId;

    private String title;
    private String first;
    private String last;
}
