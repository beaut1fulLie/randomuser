package ru.mrgeng.randomuser.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class RegisterInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long registerInfoId;

    private LocalDateTime date;
    private Byte age;

}
