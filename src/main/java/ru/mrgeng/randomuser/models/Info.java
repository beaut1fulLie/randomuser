package ru.mrgeng.randomuser.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import javax.persistence.Id;

@Data
@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class Info {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long infoId;
    private String seed;
    private Integer results;
    private Integer page;
    private String version;
}
