package ru.mrgeng.randomuser.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity(name = "timezone1")
@JsonIgnoreProperties(ignoreUnknown = true)
public class TimeZone {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long timezoneId;

    @Column(name = "time_offset")
    private String offset;
    private String description;
}
