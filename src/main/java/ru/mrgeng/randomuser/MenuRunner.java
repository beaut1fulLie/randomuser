package ru.mrgeng.randomuser;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import ru.mrgeng.randomuser.service.UserService;

import java.io.IOException;
import java.util.Scanner;

/**
 * Класс, отвечающий за исполнения приложения в командной строке и осуществляющий вывод меню и вызов методов приложения
 */
@Component
public class MenuRunner implements CommandLineRunner {

    @Autowired
    private UserService userService;

    /**
     * Основной метод, запускающий приложение в командной строке, содержит логику "клиента"
     * @param args
     * @throws Exception
     */
    @Override
    public void run(String... args) throws Exception {
        clearScreen();
        int action = -1;
        Scanner scanner = new Scanner(System.in);
        do {
            try {
                printMenu();
                action = Integer.parseInt(scanner.nextLine());
                boolean flag = true;
                int entityNumber;
                switch (action) {
                    case 1:
                        while (flag) {
                            System.out.println("Введите число записей для загрузки");
                            try {
                                entityNumber = Integer.parseInt(scanner.nextLine());
                                userService.getUsers(entityNumber);
                                scanner.nextLine();
                                flag = false;
                            } catch (NumberFormatException e) {
                                System.out.println("Неверный формат параметра. Повторите команду");
                                flag = true;
                            }
                        }
                        break;
                    case 2:
                        while(flag) {
                            System.out.println("Введите число записей для выгрузки в csv файл");
                            try {
                                entityNumber = Integer.parseInt(scanner.nextLine());
                                userService.writeUsersToCsv(entityNumber);
                                scanner.nextLine();
                                flag = false;
                            } catch (NumberFormatException e) {
                                System.out.println("Неверный формат параметра. Повторите команду");
                                flag = true;
                            }
                        }
                        break;
                    case 3:
                        printActions();
                        while (flag) {
                            String command = scanner.nextLine();
                        if (command.startsWith("getUsers")) {
                            try {
                                entityNumber = Integer.parseInt(command.split(" ")[1]);
                                userService.getUsers(entityNumber);
                                scanner.nextLine();
                                flag = false;
                            } catch (NumberFormatException | ArrayIndexOutOfBoundsException exception) {
                                System.out.println("Неверный формат параметра. Повторите команду");
                                flag = true;
                            }

                        } else if (command.startsWith("loadUsers")) {
                            try {
                                entityNumber = Integer.parseInt(command.split(" ")[1]);
                                userService.writeUsersToCsv(entityNumber);
                                scanner.nextLine();
                                flag = false;
                            } catch (NumberFormatException | ArrayIndexOutOfBoundsException exception) {
                                System.out.println("Неверный формат параметра. Повторите команду");
                                flag = true;
                            }
                        } else if (command.equals("0")) {
                            break;
                        } else {
                            System.out.println("Неверная команда! Повторите ввод");
                            flag = true;
                        }
                    }
                        break;
                    case 0: return;
                    default:
                        System.out.println("Неверный пункт меню! Повторите ввод");
                        break;
                }
                clearScreen();
            } catch (NumberFormatException e) {
                System.out.println("Ошибка формата ввода! Проверьте введенное значение");
            }
        } while (action != 0);
    }

    /**
     * Вывод главного меню приложения
     */
    private void printMenu() {
        System.out.println("Выберите пункт меню:");
        System.out.println("1. Загрузка данных из сервиса");
        System.out.println("2. Выгрузка данных из базы в файл");
        System.out.println("3. Ручной ввод команды");
        System.out.println("0. Окончание работы");
    }

    /**
     * Вывод возможных команд для ручного ввода
     */
    private void printActions() {
        System.out.println("Возможные команды:");
        System.out.println("getUsers n - получение n записей из сервиса");
        System.out.println("loadUsers n - выгрузка n первых записей в файл");
        System.out.println("0 - вернутся в меню");
    }

    /**
     * Метод для очистки экрана, предназначен для удаления лишней информации о выполненных командах
     */
    private void clearScreen() {
        try {
            if (System.getProperty("os.name").contains("Windows")) {
                new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
            }
            else {
                System.out.print("\033\143");
            }
        } catch (IOException | InterruptedException ex) {}
    }
}